CONNECTION_DISTANCE = 3.4
JOINT_DISTANCE = 4.6
COLLISION_BOX = {{-0.6, -2.5}, {0.6, 2.5}}
SELECTION_BOX = {{-0.85, -3}, {0.85, 3}}

function get_cargo_wagon_pictures_filenames()
    return {
        "__Train-position-and-length-fix__/graphics/entity/cargo-wagon/cargo-wagon-spritesheet-1.png",
        "__Train-position-and-length-fix__/graphics/entity/cargo-wagon/cargo-wagon-spritesheet-2.png",
        "__Train-position-and-length-fix__/graphics/entity/cargo-wagon/cargo-wagon-spritesheet-3.png",
        "__Train-position-and-length-fix__/graphics/entity/cargo-wagon/cargo-wagon-spritesheet-4.png"
    }
end

function get_cargo_wagon_pictures()
    return {
        priority = "very-low",
        width = 313,
        height = 239,
        back_equals_front = true,
        direction_count = 128,
        filenames = get_cargo_wagon_pictures_filenames(),
        line_length = 4,
        lines_per_file = 8,
        shift = {.8, -0.55}
    }
end

function fix_entity_physical_parameters(entity)
    entity.collision_box = COLLISION_BOX
    entity.selection_box = SELECTION_BOX
    entity.connection_distance = CONNECTION_DISTANCE
    entity.joint_distance = JOINT_DISTANCE
end

function try_fix_locomotive(locomotive)
    if not locomotive then
        return
    end

    fix_entity_physical_parameters(locomotive)
end

function try_fix_cargo_wagon(wagon)
    if not wagon then
        return
    end

    fix_entity_physical_parameters(wagon)
    wagon.pictures = get_cargo_wagon_pictures()
end


try_fix_locomotive(data.raw["locomotive"]["diesel-locomotive"])
try_fix_locomotive(data.raw["locomotive"]["diesel-locomotive-2"])
try_fix_locomotive(data.raw["locomotive"]["diesel-locomotive-3"])
try_fix_locomotive(data.raw["locomotive"]["armoured-diesel-locomotive"])
try_fix_cargo_wagon(data.raw["cargo-wagon"]["cargo-wagon"])
try_fix_cargo_wagon(data.raw["cargo-wagon"]["cargo-wagon-2"])
try_fix_cargo_wagon(data.raw["cargo-wagon"]["cargo-wagon-3"])
try_fix_cargo_wagon(data.raw["cargo-wagon"]["armoured-cargo-wagon"])
